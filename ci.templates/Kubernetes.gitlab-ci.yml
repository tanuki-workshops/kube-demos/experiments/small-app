stages:
  - 🐳build
  - 🚢deploy
  - 🔎check
  - 🛠manage
  - 🌍rules

variables:
  CONTAINER_PORT: 8080
  EXPOSED_PORT: 80
  # BASE_DOMAIN: raider.eu.ngrok.io 
  # temporary workaround or put it in the CI variables of the group

#----------------------------
# YAML Anchors (kind of DSL)
#----------------------------
.generate-manifests: &generate-manifests
- |
  envsubst < ./k8s.templates/deployment.template.yaml > ./kube/deployment.${CI_COMMIT_SHORT_SHA}.yaml
  envsubst < ./k8s.templates/ingress.template.yaml > ./kube/ingress.${CI_COMMIT_SHORT_SHA}.yaml
  envsubst < ./k8s.templates/service.template.yaml > ./kube/service.${CI_COMMIT_SHORT_SHA}.yaml
  cat -n ./kube/deployment.${CI_COMMIT_SHORT_SHA}.yaml
  cat -n ./kube/ingress.${CI_COMMIT_SHORT_SHA}.yaml
  cat -n ./kube/service.${CI_COMMIT_SHORT_SHA}.yaml

.apply: &apply
- |
  kubectl apply -f ./kube -n ${KUBE_NAMESPACE}
  #kubectl apply -f ./kube/deploy.${CI_COMMIT_SHORT_SHA}.yaml -n ${KUBE_NAMESPACE}

.delete: &delete
- |
  kubectl delete -f ./kube -n ${KUBE_NAMESPACE}
  # kubectl delete -f ./kube/deploy.${CI_COMMIT_SHORT_SHA}.yaml -n ${KUBE_NAMESPACE}
  # TODO: delete KUBE_NAMESPACE

.scale-by-3: &scale-by-3
- |
  kubectl scale --replicas=3 deploy deployment-${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG} -n ${KUBE_NAMESPACE}

.scale-by-1: &scale-by-1
- |
  kubectl scale --replicas=1 deploy deployment-${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG} -n ${KUBE_NAMESPACE}

# === Create configmap from source code file ===
# this variable `${function_code}` contains the
# source code of the function
# ex: `main.js` or `main.rb` or ...
#
# you need to define variables in `.gitlab-ci.yml``
# like that:
# variables:
#   function_code: main.js
#   function_name: main
#   function_lang: js
#   content_type: application/json;charset=UTF-8
.create_configmap_function: &create_configmap_function
- |
  kubectl create configmap function.${CI_COMMIT_SHORT_SHA} --from-file ${function_code} -o yaml --dry-run | kubectl apply -n ${KUBE_NAMESPACE} -f -

#envsubst < ./k8s.templates/ingress.template.yaml > ./kube/ingress.${CI_COMMIT_SHORT_SHA}.yaml
.generate-manifests-for-rule: &generate-manifests-for-rule
- |
  envsubst < ./k8s.deployment.strategies/ingress.add.prod.rule.template.yaml > ./kube/ingress.${CI_COMMIT_SHORT_SHA}.yaml
  envsubst < ./k8s.templates/deployment.template.yaml > ./kube/deployment.${CI_COMMIT_SHORT_SHA}.yaml
  envsubst < ./k8s.templates/service.template.yaml > ./kube/service.${CI_COMMIT_SHORT_SHA}.yaml
  #echo "👋 $KUBE_NAMESPACE 🌍 $KUBE_INGRESS_BASE_DOMAIN"
  #cat ./kube/ingress.${CI_COMMIT_SHORT_SHA}.yaml

#----------------------------
# Build Docker image
#----------------------------
.kubernetes:build:image:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: 🐳build
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  script: |
    echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA

#----------------------------
# Sample: Build Docker image
#----------------------------
#📦kaniko-build:
#  extends: .kubernetes:build:image

#----------------------------
# Deploy
#----------------------------
.kubernetes:deploy:master:
  stage: 🚢deploy
  image: registry.gitlab.com/tanuki-workshops/kube-demos/images-tools/kube
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  environment:
    name: production/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
  script:
    - *create_configmap_function
    - *generate-manifests
    - *apply
    - *scale-by-1
    #- *scale-by-3
    #- echo "http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}"

.kubernetes:deploy:preview:
  stage: 🚢deploy
  image: registry.gitlab.com/tanuki-workshops/kube-demos/images-tools/kube
  rules:
    - if: $CI_MERGE_REQUEST_IID
  environment:
    name: preview/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
    on_stop: 😢stop-preview-deploy
  script:
    - *create_configmap_function
    - *generate-manifests
    - *apply
    - *scale-by-1

💠scale:by:3:
  stage: 🛠manage
  image: registry.gitlab.com/tanuki-workshops/kube-demos/images-tools/kube
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: manual 
    - if: $CI_MERGE_REQUEST_IID
      when: manual  
  allow_failure: true
  environment:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
    on_stop: 😢stop-preview-deploy
  script:
    - *create_configmap_function
    - *generate-manifests
    - *apply
    - *scale-by-3

💠scale:by:1:
  stage: 🛠manage
  image: registry.gitlab.com/tanuki-workshops/kube-demos/images-tools/kube
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: manual 
    - if: $CI_MERGE_REQUEST_IID
      when: manual  
  allow_failure: true
  environment:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
    on_stop: 😢stop-preview-deploy
  script:
    - *create_configmap_function
    - *generate-manifests
    - *apply
    - *scale-by-1

💠switch:on:prod:rule:
  stage: 🌍rules
  image: registry.gitlab.com/tanuki-workshops/kube-demos/images-tools/kube
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual  
  allow_failure: true
  environment:
    name: prod-${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://prod.${CI_PROJECT_NAME}.${KUBE_INGRESS_BASE_DOMAIN}
    on_stop: 💠switch:off:prod:rule
  script:
    - *create_configmap_function
    - *generate-manifests-for-rule
    - *apply

💠switch:off:prod:rule:
  stage: 🌍rules
  image: registry.gitlab.com/tanuki-workshops/kube-demos/images-tools/kube
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual  
  allow_failure: true
  environment:
    name: prod-${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    action: stop
  script:
    - *generate-manifests-for-rule
    - *delete

😢stop-preview-deploy:
  stage: 🚢deploy
  image: registry.gitlab.com/tanuki-workshops/kube-demos/images-tools/kube
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
  allow_failure: true
  environment:
    #name: preview/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    action: stop
  script:
    - *generate-manifests
    - *delete

# === example ===
# 🌍post-request:
#   extends: .kubernetes:deploy:check
#   variables:
#     post_parameters: >
#       {"name":"👋 Bob Morane 😃"}
#.kubernetes:deploy:check:
#  stage: 🔎check
#  image: registry.gitlab.com/tanuki-workshops/kube-demos/images-tools/various-tools:latest
#  rules:
#    - if: $CI_COMMIT_BRANCH == "master"
#    - if: $CI_MERGE_REQUEST_IID
#  script: |
#    echo "${post_parameters}" > data.json
    #curl -d @data.json --no-progress-meter \
    #  -H "Content-Type: application/json" \
    #  -X POST http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${BASE_DOMAIN} > result.json
#    echo " "
#    echo "🌍 Request parameters:"
#    cat data.json
#    echo "🌎 Request result:"
#    http POST http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${BASE_DOMAIN} --pretty=all < data.json 
    #--pretty=all --ignore-stdin

# === example ===
# 🌎run-http-requests:
#   extends: .kubernetes:deploy:check:http:requests
#   variables:
#     post_parameters: >
#       {"name":"👋 Bill Balantine 🥃"}
.kubernetes:deploy:check:http:requests:
  stage: 🔎check
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  trigger:
    include: ci.templates/http.requests.yml

