# simple-web-app

## 👋 Disable Shared Runners

## Execute Ruby function(s) (or JavaScript, Python)

```yaml
variables:
  application_image: registry.gitlab.com/borg-collective/7of9
  # === Ruby Demo ===
  function_code: main.rb
  function_name: main
  function_lang: ruby
  content_type: plain/text;charset=UTF-8
```

And create a `main.rb` file:

```ruby
def main(params)
  return "🌍 Name= " + params.getString("name")
end
```

## Only JavaScript

```yaml
variables:
  application_image: registry.gitlab.com/borg-collective/7of9:small-js
  # === JavaScript Demo ===
  function_code: main.js
  function_name: main
  function_lang: js
  content_type: application/json;charset=UTF-8
```
